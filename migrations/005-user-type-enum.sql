BEGIN;

UPDATE schema_version SET id = 5;

CREATE TYPE user_type AS ENUM (
	'UNCONFIRMED',
	'ACTIVE_NON_PAYING',
	'ACTIVE_FREE',
	'ACTIVE_PAYING',
	'ACTIVE_DELINQUENT',
	'ADMIN',
	'UNKNOWN',
	'SUSPENDED'
);

ALTER TABLE "user" ADD COLUMN user_type2 user_type;

UPDATE "user" SET user_type2 = UPPER(user_type)::user_type;

ALTER TABLE "user" DROP COLUMN user_type;
ALTER TABLE "user" RENAME COLUMN user_type2 TO user_type;
ALTER TABLE "user" ALTER COLUMN user_type SET NOT NULL;

COMMIT;
