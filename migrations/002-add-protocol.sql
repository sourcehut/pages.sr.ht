BEGIN;

CREATE TYPE protocol AS ENUM ('https', 'gemini');

ALTER TABLE sites DROP CONSTRAINT sites_domain_key;

ALTER TABLE sites ADD COLUMN protocol protocol NOT NULL DEFAULT 'https';

ALTER TABLE sites ALTER COLUMN protocol DROP DEFAULT;

ALTER TABLE sites
	ADD CONSTRAINT sites_domain_protocol_key
	UNIQUE (domain, protocol);

UPDATE schema_version SET id = 2;

COMMIT;
