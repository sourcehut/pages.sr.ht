BEGIN;

ALTER TABLE sites ADD COLUMN not_found varchar;

UPDATE schema_version SET id = 3;

COMMIT;
