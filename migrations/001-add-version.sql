BEGIN;

CREATE TABLE schema_version (
	id integer NOT NULL
);

INSERT INTO schema_version VALUES (1);

COMMIT;
