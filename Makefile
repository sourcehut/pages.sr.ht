all: server daily

server:
	go generate ./...
	go build

daily:
	cd pagessrht-daily && go build

.PHONY: all server daily
