# pages.sr.ht-daily

This tool finds old, unreachable site versions and schedules them for deletion
via a bucket lifecycle policy. It cleans out at most 1000 versions per run, as
this is the default limit in both AWS S3 and Ceph Radosgw.

The lifecycle policies are (usually) evaluated once per day. E.g. Ceph by
defaults start at midnight. Hence, if possible, it makes sense to run the tool
sometime close before that.

Build with `go build` and run the resulting executable on pages.sr.ht.
