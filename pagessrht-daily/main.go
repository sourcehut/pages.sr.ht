package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"git.sr.ht/~sircmpwn/core-go/config"
	"git.sr.ht/~sircmpwn/core-go/s3"
	_ "github.com/lib/pq"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/lifecycle"
)

type Site struct {
	domain string
	key    string
}

type Version struct {
	id  string
	key string
}

func NewSite(key string) Site {
	return Site{
		domain: path.Base(path.Clean(key)),
		key:    key,
	}
}

func NewVersion(key string) Version {
	return Version{
		id:  path.Base(path.Clean(key)),
		key: key,
	}
}

func contains(list []string, item string) bool {
	for _, i := range list {
		if i == item {
			return true
		}
	}
	return false
}

func getCurrentVersions(db *sql.DB, domain string) ([]string, error) {
	q := `SELECT version FROM "sites" WHERE domain = $1`
	rows, err := db.Query(q, domain)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var currentVersions []string

	for rows.Next() {
		var current string
		if err := rows.Scan(&current); err != nil {
			return nil, err
		}
		currentVersions = append(currentVersions, current)
	}
	return currentVersions, nil
}

func main() {
	log.Println("Starting...")

	conf := config.LoadConfig(":5112")

	pgcs, ok := conf.Get("pages.sr.ht", "connection-string")
	if !ok {
		log.Fatalf("No connection string provided in config.ini")
	}

	db, err := sql.Open("postgres", pgcs)
	if err != nil {
		log.Fatalf("Failed to open a database connection: %v", err)
	}

	mc, err := s3.NewClient(conf)
	if err != nil {
		log.Fatal(err)
	}

	bucket, _ := conf.Get("pages.sr.ht", "s3-bucket")
	prefix, _ := conf.Get("pages.sr.ht", "s3-prefix")

	if prefix != "" && !strings.HasSuffix(prefix, "/") {
		prefix = prefix + "/"
	}

	ctx := context.Background()
	listOpts := minio.ListObjectsOptions{
		Prefix: prefix + "sites/",
	}

	var (
		wg sync.WaitGroup
		lp sync.WaitGroup
	)

	ch := make(chan Version, 100)

	lp.Add(1)
	go func(c chan Version, bucket string) {
		// The lifecycle is always set for the entire bucket, potentially overwriting
		// existing rules, so it should only be touched from a single goroutine.

		defer lp.Done()

		config := lifecycle.NewConfiguration()

		for v := range c {
			if len(config.Rules) == 1000 {
				break
			}

			log.Printf("LC: Expiring %s", v.key)

			config.Rules = append(config.Rules, lifecycle.Rule{
				ID:     fmt.Sprintf("expire-%04d", len(config.Rules)),
				Status: "Enabled",
				Expiration: lifecycle.Expiration{
					Days: 1,
				},
				RuleFilter: lifecycle.Filter{
					Prefix: v.key,
				},
			})
		}

		// Even if there were no rules generated, apply to remove previous rules.
		err := mc.SetBucketLifecycle(context.Background(), bucket, config)
		if err != nil {
			log.Printf("Error setting lifecycle policy: %s", err.Error())
			os.Exit(1)
		} else {
			log.Printf("LC: applied lifecycle with %d rules", len(config.Rules))
			os.Exit(0)
		}
	}(ch, bucket)

	for entry := range mc.ListObjects(ctx, bucket, listOpts) {
		// Space goroutines a bit apart...
		time.Sleep(1 * time.Second)

		wg.Add(1)
		go func(site Site, c chan Version) {
			defer wg.Done()
			log.Printf("%s: processing...\n", site.domain)

			var versions []Version
			lo := minio.ListObjectsOptions{
				Prefix: site.key,
			}

			for v := range mc.ListObjects(ctx, bucket, lo) {
				versions = append(versions, NewVersion(v.Key))
			}

			// The listed versions can potentially contain one
			// which is being currently uploaded and is not yet in
			// the database. Make sure to wait for the timeout
			// period of upload requests (plus a little) so that
			// the version shows up in the database.
			// If yet another upload starts during that time, no
			// problem: the version it created is not in our
			// listing and will not be touched.
			log.Printf("%s: waiting for potential uploads to settle...\n", site.domain)
			time.Sleep(35 * time.Second)

			current, err := getCurrentVersions(db, site.domain)
			if err != nil {
				log.Fatalf("%s: failed to get current versions: %s", site.domain, err.Error())
			}

			log.Printf("%s: current versions: %s", site.domain, strings.Join(current, ", "))

			for _, v := range versions {
				if contains(current, v.id) {
					continue
				}
				log.Printf("%s: scheduling %s for deletion", site.domain, v.key)
				c <- v
			}
		}(NewSite(entry.Key), ch)
	}
	wg.Wait()
	close(ch)
	lp.Wait()

	log.Println("Done.")
}
