package account

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"path"

	"git.sr.ht/~sircmpwn/core-go/config"
	"git.sr.ht/~sircmpwn/core-go/database"
	work "git.sr.ht/~sircmpwn/dowork"
	minio "github.com/minio/minio-go/v7"

	"git.sr.ht/~sircmpwn/pages.sr.ht/s3"
)

type contextKey struct {
	name string
}

var ctxKey = &contextKey{"account"}

func Middleware(queue *work.Queue) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), ctxKey, queue)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// Schedules a user account deletion.
func Delete(ctx context.Context, userID int, username string) {
	queue, ok := ctx.Value(ctxKey).(*work.Queue)
	if !ok {
		panic("No account worker for this context")
	}

	conf := config.ForContext(ctx)
	bucket, _ := conf.Get("pages.sr.ht", "s3-bucket")
	prefix, _ := conf.Get("pages.sr.ht", "s3-prefix")

	mc := s3.ForContext(ctx)
	task := work.NewTask(func(ctx context.Context) error {
		log.Printf("Processing deletion of user account %d %s", userID, username)

		var domains []string
		if err := database.WithTx(ctx, &sql.TxOptions{
			Isolation: 0,
			ReadOnly:  true,
		}, func(tx *sql.Tx) error {
			rows, err := tx.QueryContext(ctx, `
				SELECT domain FROM sites WHERE user_id = $1
			`, userID)

			if err != nil {
				return err
			}

			for rows.Next() {
				var domain string
				if err := rows.Scan(&domain); err != nil {
					return err
				}
				domains = append(domains, domain)
			}

			return rows.Err()
		}); err != nil {
			return err
		}

		for _, domain := range domains {
			s3path := path.Join(prefix, "sites", domain)

			och := mc.ListObjects(context.Background(), bucket, minio.ListObjectsOptions{
				Prefix:    s3path,
				Recursive: true,
			})

			for err := range mc.RemoveObjects(context.Background(),
				bucket, och, minio.RemoveObjectsOptions{}) {
				log.Printf("Failed to remove S3 object %q: %v", err.ObjectName, err.Err)
			}
		}

		if err := database.WithTx(ctx, nil, func(tx *sql.Tx) error {
			_, err := tx.ExecContext(ctx, `
				DELETE FROM "user" WHERE id = $1;
			`, userID)
			return err
		}); err != nil {
			return err
		}

		log.Printf("Deletion of user account %d %s complete", userID, username)
		return nil
	})
	queue.Enqueue(task)
	log.Printf("Enqueued deletion of user account %d %s", userID, username)
}
