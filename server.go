package main

import (
	"context"
	"crypto/tls"
	"database/sql"
	"errors"
	"io"
	"log"
	"mime"
	"net"
	"net/http"
	"path"
	"strings"
	"time"

	"git.sr.ht/~adnano/go-gemini"
	"git.sr.ht/~adnano/go-gemini/certificate"
	"git.sr.ht/~sircmpwn/core-go/config"
	"git.sr.ht/~sircmpwn/core-go/database"
	cores3 "git.sr.ht/~sircmpwn/core-go/s3"
	"git.sr.ht/~sircmpwn/core-go/server"
	"git.sr.ht/~sircmpwn/core-go/webhooks"
	work "git.sr.ht/~sircmpwn/dowork"
	"github.com/99designs/gqlgen/graphql"
	"github.com/CAFxX/httpcompression"
	"github.com/go-chi/chi/v5"
	"github.com/minio/minio-go/v7"
	"github.com/pires/go-proxyproto"
	h2proxy "github.com/pires/go-proxyproto/helper/http2"
	"github.com/vaughan0/go-ini"

	"git.sr.ht/~sircmpwn/pages.sr.ht/account"
	"git.sr.ht/~sircmpwn/pages.sr.ht/graph"
	"git.sr.ht/~sircmpwn/pages.sr.ht/graph/api"
	"git.sr.ht/~sircmpwn/pages.sr.ht/graph/model"
	"git.sr.ht/~sircmpwn/pages.sr.ht/s3"
)

func main() {
	appConfig := config.LoadConfig(":5112")

	gqlConfig := api.Config{Resolvers: &graph.Resolver{}}
	gqlConfig.Directives.Private = server.Private
	gqlConfig.Directives.Internal = server.Internal
	gqlConfig.Directives.Access = func(ctx context.Context, obj interface{},
		next graphql.Resolver, scope model.AccessScope,
		kind model.AccessKind) (interface{}, error) {
		return server.Access(ctx, obj, next, scope.String(), kind.String())
	}
	schema := api.NewExecutableSchema(gqlConfig)

	scopes := make([]string, len(model.AllAccessScope))
	for i, s := range model.AllAccessScope {
		scopes[i] = s.String()
	}

	mc, err := cores3.NewClient(appConfig)
	if err != nil {
		log.Fatal(err)
	}

	pgcs, ok := appConfig.Get("pages.sr.ht", "connection-string")
	if !ok {
		log.Fatalf("No connection string provided in config.ini")
	}

	db, err := sql.Open("postgres", pgcs)
	if err != nil {
		log.Fatalf("Failed to open a database connection: %v", err)
	}

	hsrv := ServeHTTP(appConfig, db, mc)
	gemsrv := ServeGemini(appConfig, db, mc)
	go gemsrv.ListenAndServe(context.Background())

	queueSize := config.GetInt(appConfig, "pages.sr.ht",
		"account-del-queue-size", config.DefaultQueueSize)
	accountQueue := work.NewQueue("account", queueSize)
	webhookQueue := webhooks.NewQueue(schema, appConfig)

	gsrv := server.NewServer("pages.sr.ht", appConfig).
		WithDefaultMiddleware().
		WithMiddleware(
			s3.Middleware(mc),
			account.Middleware(accountQueue),
			webhooks.Middleware(webhookQueue),
		).
		WithSchema(schema, scopes).
		WithQueues(accountQueue, webhookQueue.Queue)

	gsrv.AnonRouter().Get("/", func(w http.ResponseWriter, r *http.Request) {
		redir, _ := appConfig.Get("pages.sr.ht", "site-info")
		if redir != "" {
			http.Redirect(w, r, redir, http.StatusFound)
		} else {
			http.NotFound(w, r)
		}
	})

	gsrv.AnonRouter().Get("/domain", func(w http.ResponseWriter, r *http.Request) {
		domain := r.URL.Query().Get("domain")
		ctx := database.Context(r.Context(), db)
		if err := database.WithTx(ctx, &sql.TxOptions{
			ReadOnly:  true,
			Isolation: 0,
		}, func(tx *sql.Tx) error {
			var id int
			row := tx.QueryRowContext(ctx, `
				SELECT id FROM sites WHERE domain = $1 AND protocol = 'https';
			`, domain)
			return row.Scan(&id)
		}); err != nil {
			http.Error(w, "404 Not found", 404)
			return
		}
		w.Write([]byte("200 OK"))
	})

	handlePublish := func(w http.ResponseWriter, r *http.Request) {
		ctx := database.Context(r.Context(), db)
		r = r.WithContext(ctx)
		domain := chi.URLParam(r, "domain")
		if domain == "" {
			http.Error(w, "Invalid domain", 400)
			return
		}

		subdir := "/" + chi.URLParam(r, "*")

		proto := r.FormValue("protocol")
		if r.MultipartForm != nil {
			defer r.MultipartForm.RemoveAll()
		}
		if proto == "" {
			proto = "HTTPS"
		}
		var protocol model.Protocol
		err := protocol.UnmarshalGQL(proto)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		f, h, err := r.FormFile("content")
		if err != nil {
			http.Error(w, "Expected 'content' parameter as file", 400)
			return
		}
		defer f.Close()

		site, err := (&graph.Resolver{}).Mutation().Publish(r.Context(),
			domain, graphql.Upload{
				File:     f,
				Filename: h.Filename,
				Size:     h.Size,
			}, &protocol, &subdir, nil)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		w.Write([]byte(site.Version + "\n"))
	}
	gsrv.Router().Post("/publish/{domain}", handlePublish)
	gsrv.Router().Post("/publish/{domain}/*", handlePublish)

	gsrv.Run()
	hsrv.Close()
	gemsrv.Close()
}

func ServeHTTP(conf ini.File, db *sql.DB, mc *minio.Client) *http.Server {
	bucket, _ := conf.Get("pages.sr.ht", "s3-bucket")
	prefix, _ := conf.Get("pages.sr.ht", "s3-prefix")
	addr := ":5012"

	compressionHandler, err := httpcompression.DefaultAdapter()
	if err != nil {
		panic(err)
	}

	srv := &http.Server{}
	srv.Handler = compressionHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sandbox := strings.Join([]string{
			"allow-downloads",
			"allow-forms",
			"allow-pointer-lock",
			"allow-presentation",
			"allow-same-origin",
			"allow-scripts",
			"allow-popups",
		}, " ")
		w.Header().Add("Content-Security-Policy", strings.Join([]string{
			"default-src 'self' data: blob:",
			"script-src 'self' 'unsafe-eval' 'unsafe-inline'",
			"style-src 'self' 'unsafe-inline'",
			"worker-src 'self'",
			"frame-src https:",
			"img-src data: https:",
			"media-src https:",
			"object-src 'none'",
			"sandbox " + sandbox,
		}, "; ")+";")

		// CORS support
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Request-Methods", "GET, HEAD, OPTIONS")

		var version string
		var notFoundPath *string
		ctx := database.Context(r.Context(), db)
		if err := database.WithTx(ctx, &sql.TxOptions{
			ReadOnly:  true,
			Isolation: 0,
		}, func(tx *sql.Tx) error {
			row := tx.QueryRowContext(ctx, `
				SELECT version, not_found
				FROM sites
				WHERE domain = $1 AND protocol = 'https';
			`, r.Host)
			if err := row.Scan(&version, &notFoundPath); err != nil {
				return err
			}
			return nil
		}); err == sql.ErrNoRows {
			http.Error(w, "404 Site not found\n", 404)
			return
		} else if err != nil {
			panic(err)
		}

		paths := []string{
			r.URL.Path,
			path.Join(r.URL.Path, "index.html"),
			path.Join(r.URL.Path, "index.xml"),
			path.Join(r.URL.Path, "index.xhtml"),
		}

		if notFoundPath != nil {
			paths = append(paths, *notFoundPath)
		}

		var s3path string
		var object *minio.Object
		var objectInfo minio.ObjectInfo
		found := false
		for _, cand := range paths {
			s3path = path.Join(prefix, "sites", r.Host, version, cand)
			var err error
			objectInfo, err = mc.StatObject(ctx, bucket, s3path, minio.StatObjectOptions{})
			if err != nil {
				var errResp minio.ErrorResponse
				if errors.As(err, &errResp) && errResp.StatusCode == http.StatusNotFound {
					continue
				}
				log.Printf("failed to stat minio object: %v", err)
				http.Error(w, "Internal error", http.StatusInternalServerError)
				return
			}
			object, err = mc.GetObject(ctx, bucket, s3path, minio.GetObjectOptions{})
			if err != nil {
				panic(err)
			}
			found = notFoundPath == nil || cand != *notFoundPath
			break
		}

		defer object.Close()
		if !found {
			if notFoundPath == nil {
				http.Error(w, "404 Page not found\n", http.StatusNotFound)
				return
			}
			if object == nil {
				http.Error(w, "You have a configured 404 page which does not exist\n", http.StatusNotFound)
				return
			}
			mimetype := mime.TypeByExtension(path.Ext(*notFoundPath))
			if mimetype != "" {
				w.Header().Add("Content-Type", mimetype)
			}
			w.WriteHeader(http.StatusNotFound)
			if _, err := io.Copy(w, object); err != nil {
				log.Println(err.Error())
			}
			return
		}
		cc := objectInfo.Metadata.Get("Cache-Control")
		if cc != "" {
			w.Header().Set("Cache-Control", cc)
		}
		http.ServeContent(w, r, s3path, objectInfo.LastModified, object)
	}))

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Failed to start HTTP listener: %v", err)
	}

	log.Printf("Running HTTP server on %v", addr)
	go h2proxy.NewServer(srv, nil).Serve(&proxyproto.Listener{Listener: ln})

	return srv
}

func ServeGemini(conf ini.File, db *sql.DB, mc *minio.Client) *gemini.Server {
	bucket, _ := conf.Get("pages.sr.ht", "s3-bucket")
	prefix, _ := conf.Get("pages.sr.ht", "s3-prefix")
	certdir, _ := conf.Get("pages.sr.ht", "gemini-certs")

	certificates := &certificate.Store{}
	certificates.Register("*")
	if err := certificates.Load(certdir); err != nil {
		log.Fatal(err)
	}

	mux := &gemini.Mux{}
	mux.HandleFunc("/", func(ctx context.Context, w gemini.ResponseWriter, r *gemini.Request) {
		var version string
		ctx = database.Context(ctx, db)
		if err := database.WithTx(ctx, &sql.TxOptions{
			ReadOnly:  true,
			Isolation: 0,
		}, func(tx *sql.Tx) error {
			row := tx.QueryRowContext(ctx, `
				SELECT version
				FROM sites
				WHERE domain = $1 AND protocol = 'gemini';
			`, r.URL.Hostname())
			if err := row.Scan(&version); err != nil {
				return err
			}
			return nil
		}); err == sql.ErrNoRows {
			w.WriteHeader(gemini.StatusNotFound, "Site not found")
			return
		} else if err != nil {
			panic(err)
		}

		paths := []string{
			r.URL.Path,
			path.Join(r.URL.Path, "index.gmi"),
		}

		var object *minio.Object
		for _, cand := range paths {
			s3path := path.Join(prefix, "sites", r.URL.Hostname(), version, cand)
			_, err := mc.StatObject(ctx, bucket, s3path, minio.StatObjectOptions{})
			if err != nil {
				continue
			}
			object, err = mc.GetObject(ctx, bucket, s3path, minio.GetObjectOptions{})
			if err != nil {
				panic(err)
			}
			break
		}

		if object == nil {
			w.WriteHeader(gemini.StatusNotFound, "File not found")
			return
		}

		defer object.Close()
		ext := path.Ext(r.URL.Path)
		if ext == ".gmi" {
			w.SetMediaType("text/gemini")
		} else {
			w.SetMediaType(mime.TypeByExtension(ext))
		}
		if _, err := io.Copy(w, object); err != nil {
			w.WriteHeader(gemini.StatusTemporaryFailure, err.Error())
			return
		}
	})

	srv := &gemini.Server{
		Handler:      mux,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 1 * time.Minute,
		GetCertificate: func(scope string) (*tls.Certificate, error) {
			return certificates.Get(scope)
		},
	}

	return srv
}
