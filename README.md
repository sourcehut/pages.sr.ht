This repository contains the code for the sr.ht web hosting service. For
instructions on deploying or contributing to this project, visit the manual
here:

https://man.sr.ht/pages.sr.ht/installation.md
