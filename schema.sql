BEGIN;

CREATE TABLE schema_version (
	id integer NOT NULL
);

INSERT INTO schema_version VALUES (6);

CREATE TYPE user_type AS ENUM (
	'PENDING',
	'USER',
	'ADMIN',
	'SUSPENDED'
);

CREATE TABLE "user" (
	id serial PRIMARY KEY,
	created timestamp NOT NULL,
	updated timestamp NOT NULL,
	username varchar(256) NOT NULL UNIQUE,
	email varchar(256) NOT NULL,
	user_type user_type NOT NULL,
	url varchar,
	location varchar,
	bio varchar,
	suspension_notice varchar
);

CREATE TYPE protocol AS ENUM ('https', 'gemini');

CREATE TABLE sites (
	id serial PRIMARY KEY,
	created timestamp NOT NULL,
	updated timestamp NOT NULL,
	user_id integer NOT NULL references "user"(id),
	domain varchar NOT NULL,
	protocol protocol NOT NULL,
	version varchar NOT NULL,
	not_found varchar,
	UNIQUE (domain, protocol)
);

CREATE TYPE webhook_event AS ENUM (
	'SITE_PUBLISHED',
	'SITE_UNPUBLISHED'
);

CREATE TYPE auth_method AS ENUM (
	'OAUTH_LEGACY',
	'OAUTH2',
	'COOKIE',
	'INTERNAL',
	'WEBHOOK'
);

CREATE TABLE gql_user_wh_sub (
	id serial PRIMARY KEY,
	created timestamp NOT NULL,
	events webhook_event[] NOT NULL check (array_length(events, 1) > 0),
	url varchar NOT NULL,
	query varchar NOT NULL,

	auth_method auth_method NOT NULL check (auth_method in ('OAUTH2', 'INTERNAL')),
	token_hash varchar(128) check ((auth_method = 'OAUTH2') = (token_hash IS NOT NULL)),
	grants varchar,
	client_id uuid,
	expires timestamp check ((auth_method = 'OAUTH2') = (expires IS NOT NULL)),
	node_id varchar check ((auth_method = 'INTERNAL') = (node_id IS NOT NULL)),

	user_id integer NOT NULL references "user"(id)
);

CREATE INDEX gql_user_wh_sub_token_hash_idx ON gql_user_wh_sub (token_hash);

CREATE TABLE gql_user_wh_delivery (
	id serial PRIMARY KEY,
	uuid uuid NOT NULL,
	date timestamp NOT NULL,
	event webhook_event NOT NULL,
	subscription_id integer NOT NULL references gql_user_wh_sub(id) ON DELETE CASCADE,
	request_body varchar NOT NULL,
	response_body varchar,
	response_headers varchar,
	response_status integer
);

COMMIT;
