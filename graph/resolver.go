package graph

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/url"

	"git.sr.ht/~sircmpwn/core-go/config"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct{}

// via https://github.com/distribution/distribution, Apache 2.0

// limitReader returns a new reader limited to n bytes. Unlike io.LimitReader,
// this returns an error when the limit reached.
func limitReader(r io.Reader, n int64) io.Reader {
	return &limitedReader{r: r, n: n}
}

// limitedReader implements a reader that errors when the limit is reached.
type limitedReader struct {
	r   io.Reader // underlying reader
	n   int64     // max bytes remaining
	err error     // sticky error
}

func (l *limitedReader) Read(p []byte) (n int, err error) {
	if l.err != nil {
		return 0, l.err
	}
	if len(p) == 0 {
		return 0, nil
	}
	// If they asked for a 32KB byte read but only 5 bytes are
	// remaining, no need to read 32KB. 6 bytes will answer the
	// question of the whether we hit the limit or go past it.
	if int64(len(p)) > l.n+1 {
		p = p[:l.n+1]
	}
	n, err = l.r.Read(p)

	if int64(n) <= l.n {
		l.n -= int64(n)
		l.err = err
		return n, err
	}

	n = int(l.n)
	l.n = 0

	l.err = errors.New("storage: read exceeds limit")
	return n, l.err
}

func checkDNSRecords(ctx context.Context, domain string) error {
	conf := config.ForContext(ctx)
	origin, _ := conf.Get("pages.sr.ht", "origin")
	originURL, err := url.Parse(origin)
	if err != nil {
		panic(fmt.Errorf("failed to parse pages.sr.ht origin URL: %v", err))
	}

	var resolver net.Resolver
	wantIPAddrs, err := resolver.LookupIPAddr(ctx, originURL.Hostname())
	if err != nil {
		return fmt.Errorf("failed to lookup pages.sr.ht origin IP addresses: %v", err)
	}

	gotIPAddrs, err := resolver.LookupIPAddr(ctx, domain)
	if err != nil {
		return fmt.Errorf("failed to lookup IP addresses for domain %q: %v", domain, err)
	} else if len(gotIPAddrs) == 0 {
		return fmt.Errorf("failed to lookup IP addresses for domain %q: no DNS record found", domain)
	}

	for _, gotAddr := range gotIPAddrs {
		found := false
		for _, wantAddr := range wantIPAddrs {
			if gotAddr.IP.Equal(wantAddr.IP) {
				found = true
				break
			}
		}
		if !found {
			recordType := "A"
			if gotAddr.IP.To4() == nil {
				recordType = "AAAA"
			}
			return fmt.Errorf("DNS record type %v for %q resolves to IP address %v, which doesn't match %v", recordType, domain, gotAddr.IP, originURL.Host)
		}
	}

	return nil
}
