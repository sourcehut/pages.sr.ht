package model

import (
	"context"
	"database/sql"
	"strconv"
	"time"

	sq "github.com/Masterminds/squirrel"

	"git.sr.ht/~sircmpwn/core-go/database"
	"git.sr.ht/~sircmpwn/core-go/model"
)

type Site struct {
	ID       int       `json:"id"`
	Created  time.Time `json:"created"`
	Updated  time.Time `json:"updated"`
	Domain   string    `json:"domain"`
	Protocol Protocol  `json:"protocol"`
	Version  string    `json:"version"`
	NotFound *string   `json:"notFound"`

	alias  string
	fields *database.ModelFields
}

func (site *Site) As(alias string) *Site {
	site.alias = alias
	return site
}

func (site *Site) Alias() string {
	return site.alias
}

func (site *Site) Table() string {
	return `sites`
}

func (site *Site) Fields() *database.ModelFields {
	if site.fields != nil {
		return site.fields
	}
	site.fields = &database.ModelFields{
		Fields: []*database.FieldMap{
			{"id", "id", &site.ID},
			{"created", "created", &site.Created},
			{"updated", "updated", &site.Updated},
			{"domain", "domain", &site.Domain},
			{"protocol", "protocol", &site.Protocol},
			{"version", "version", &site.Version},
			{"not_found", "notFound", &site.NotFound},

			// Always fetch:
			{"id", "", &site.ID},
		},
	}
	return site.fields
}

func (site *Site) QueryWithCursor(ctx context.Context, runner sq.BaseRunner,
	q sq.SelectBuilder, cur *model.Cursor) ([]*Site, *model.Cursor) {
	var (
		err  error
		rows *sql.Rows
	)

	if cur.Next != "" {
		next, _ := strconv.ParseInt(cur.Next, 10, 64)
		q = q.Where(database.WithAlias(site.alias, "id")+"<= ?", next)
	}
	q = q.
		OrderBy(database.WithAlias(site.alias, "id")).
		Limit(uint64(cur.Count + 1))

	if rows, err = q.RunWith(runner).QueryContext(ctx); err != nil {
		panic(err)
	}
	defer rows.Close()

	var sites []*Site
	for rows.Next() {
		var site Site
		if err := rows.Scan(database.Scan(ctx, &site)...); err != nil {
			panic(err)
		}
		sites = append(sites, &site)
	}

	if len(sites) > cur.Count {
		cur = &model.Cursor{
			Count:  cur.Count,
			Next:   strconv.Itoa(sites[len(sites)-1].ID),
			Search: cur.Search,
		}
		sites = sites[:cur.Count]
	} else {
		cur = nil
	}

	return sites, cur
}
