#!/usr/bin/python3
"""Use canonical user ID"""

import sqlalchemy as sa
from srht.config import cfg
from srht.database import db, DbSession

db = DbSession(cfg("pages.sr.ht", "connection-string"))
db.init()


# These tables all have a column referencing "user"(id)
tables = [
    ("gql_user_wh_sub", "user_id"),
    ("sites", "user_id"),
]

def upgrade():
    # Drop foreign key constraints and update user IDs
    for (table, col) in tables:
        db.session.execute(f"""
        ALTER TABLE {table} DROP CONSTRAINT {table}_{col}_fkey;
        UPDATE {table} t SET {col} = u.remote_id FROM "user" u WHERE u.id = t.{col};
        """)

    # Update primary key
    db.session.execute("""
    ALTER TABLE "user" DROP CONSTRAINT user_pkey;
    ALTER TABLE "user" DROP CONSTRAINT user_remote_id_key;
    ALTER TABLE "user" RENAME COLUMN id TO old_id;
    ALTER TABLE "user" RENAME COLUMN remote_id TO id;
    ALTER TABLE "user" ADD PRIMARY KEY (id);
    ALTER TABLE "user" ADD UNIQUE (old_id);
    """)

    # Add foreign key constraints
    for (table, col) in tables:
        db.session.execute(f"""
        ALTER TABLE {table} ADD CONSTRAINT {table}_{col}_fkey FOREIGN KEY ({col}) REFERENCES "user"(id) ON DELETE CASCADE;
        """)

    db.session.commit()


def downgrade():
    # Drop foreign key constraints and update user IDs
    for (table, col) in tables:
        db.session.execute(f"""
        ALTER TABLE {table} DROP CONSTRAINT {table}_{col}_fkey;
        UPDATE {table} t SET {col} = u.old_id FROM "user" u WHERE u.id = t.{col};
        """)

    # Update primary key
    db.session.execute("""
    ALTER TABLE "user" DROP CONSTRAINT user_pkey;
    ALTER TABLE "user" DROP CONSTRAINT user_old_id_key;
    ALTER TABLE "user" RENAME COLUMN id TO remote_id;
    ALTER TABLE "user" RENAME COLUMN old_id TO id;
    ALTER TABLE "user" ADD PRIMARY KEY (id);
    ALTER TABLE "user" ADD UNIQUE (remote_id);
    """)

    # Add foreign key constraints
    for (table, col) in tables:
        db.session.execute(f"""
        ALTER TABLE {table} ADD CONSTRAINT {table}_{col}_fkey FOREIGN KEY ({col}) REFERENCES "user"(id) ON DELETE CASCADE;
        """)
    
    db.session.commit()

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2 or (sys.argv[1] != "upgrade" and sys.argv[1] != "downgrade"):
        print(f"usage: {sys.argv[0]} upgrade|downgrade")
        sys.exit(1)
    if sys.argv[1] == "upgrade":
        upgrade()
        print("Upgrade complete")
    else:
        downgrade()
        print("Downgrade complete")
