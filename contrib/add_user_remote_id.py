#!/usr/bin/python3
"""Add user.remote_id"""

import sqlalchemy as sa
from srht.config import cfg
from srht.crypto import internal_anon
from srht.database import db, DbSession, Base
from srht.graphql import exec_gql

db = DbSession(cfg("pages.sr.ht", "connection-string"))
db.init()

class User(Base):
    __tablename__ = "user"
    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.Unicode(256), index=True, unique=True)
    remote_id = sa.Column(sa.Integer, unique=True)

def upgrade():
    db.session.execute("""ALTER TABLE "user" ADD COLUMN remote_id integer UNIQUE""")

    for user in User.query:
        user.remote_id = fetch_user_id(user.username)
        print(f"~{user.username} id: {user.id} -> {user.remote_id}")
    db.session.commit()

    db.session.execute("""ALTER TABLE "user" ALTER COLUMN remote_id SET NOT NULL""")
    db.session.commit()

def downgrade():
    db.session.execute("""ALTER TABLE "user" DROP COLUMN remote_id""")
    db.session.commit()

def fetch_user_id(username):
    resp = exec_gql("meta.sr.ht",
            "query($username: String!) { user(username: $username) { id } }",
            user=internal_anon,
            username=username)
    return resp["user"]["id"]

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2 or (sys.argv[1] != "upgrade" and sys.argv[1] != "downgrade"):
        print(f"usage: {sys.argv[0]} upgrade|downgrade")
        sys.exit(1)
    if sys.argv[1] == "upgrade":
        upgrade()
        print("Upgrade complete")
    else:
        downgrade()
        print("Downgrade complete")
